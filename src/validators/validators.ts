export const validators = {
    empIDValidator(value: string): boolean {
        if (/TY*/.test(value)) {
            return true
        } else {
            return false
        }
    },
    institutionalUniversityValidator(value: string): boolean {
        if (/^[A-Za-z\s]{1,}[.]{0,1}[A-Za-z\s]{0,}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    percentageValidator(value: string): boolean {
        if (parseFloat(value) >= 0 && parseFloat(value) < 101) {
            return true
        } else {
            return false
        }
    },
    nameValidator(value: string): boolean {
        if (/^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    numberValidator(value: string): boolean {
        if (/^\d{10}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    lastNameValidator(value: string): boolean {
        if (/^[a-zA-Z]*$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    emailValidator(value: string): boolean {
        if (/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    textValidator(value: string): boolean {
        if (/^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/.test(value)) {
            return true
        } else {
            return false
        }
    },
    fieldValidator(value: string): boolean {
        if (value.length > 0) {
            return true
        } else {
            return false
        }
    }
}

