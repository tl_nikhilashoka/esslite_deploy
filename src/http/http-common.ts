import axios from 'axios';

export const HTTP = axios.create({
  baseURL: `https://esslite-app.herokuapp.com/`
})