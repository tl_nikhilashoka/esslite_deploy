interface ISkillArray {
    stack: string;
    skills: Array<ISkillObj>;
}
interface ISkillObj {
    type: string;
    items: Array<ISkillItem>;
  }
  interface ISkillItem {
    skillName: string;
    ratings: number;
  }
  interface ISubSkillObj {
    skillName: string;
    ratings: number;
  }