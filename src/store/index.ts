import { createStore } from 'vuex'

export default createStore({
  state: {
    loading: false,
    employeeid: 'tyss121',
    employeeData: {},
    university: 'VTU',
    showElements: {
      showBranch: true,
      showSummary: true,
      showProject: true,
      showAchievements: true,
      showCertifications: true,
      showEducation: true,
    },
    showList: {
      LanguagesList: [],
      AchievementsList: [],
    },
    eduDetails: '',
    showEduItems: [],
    showTechItems: [],
    projectItems: [],
    showCertItems: [],
    summaryList: [],
    skillExamples: [],
    experience: {
      totalExp: '',
      relExp: '',
      totalMonth: '',
      relMonth: '',
    },
    display: {
      dispEdu: true,
      dispCert: true,
      dispProject: true,
      dispTech: true,
    },
    validateProfile: {
      first: true,
      last: true,
    }
  },
  mutations: {
    updateEmployeeData(state, payload) {
      state.employeeData[payload.type] = payload.value
    },
    updateEmployeeEduData(state, payload) {
      state.eduDetails = payload.value
    },
    updateShowEduItems(state, payload) {
      state.showEduItems = payload.value
    },
    updateShowTechItems(state, payload) {
      state.showTechItems = payload.value
    },
    updateProjectItems(state, payload) {
      state.projectItems = payload.value
    },
    updateCertItems(state, payload) {
      state.showCertItems = payload.value
    },
    updateSummaryItems(state, payload) {
      state.summaryList = payload.value
    },
    updateUniversity(state, payload) {
      state.university = payload.value
    },
    updateLoading(state, payload) {
      state.loading = payload.value
    },
    updateAllEmployeeData(state, payload) {
      state.employeeData = payload.value
    },
  },
  actions: {
    updateEmployeeData(context, payload) {
      context.commit('updateEmployeeData', payload)
    },
    updateEmployeeEduData(context, payload) {
      context.commit('updateEmployeeEduData', payload)
    },
    updateShowEduItems(context, payload) {
      context.commit('updateShowEduItems', payload)
    },
    updateShowTechItems(context, payload) {
      context.commit('updateShowTechItems', payload)
    },
    updateProjectItems(context, payload) {
      context.commit('updateProjectItems', payload)
    },
    updateCertItems(context, payload) {
      context.commit('updateCertItems', payload)
    },
    updateSummaryItems(context, payload) {
      context.commit('updateSummaryItems', payload)
    },
    updateUniversity(context, payload) {
      context.commit('updateUniversity', payload)
    },
    updateLoading(context, payload) {
      context.commit('updateLoading', payload)
    },
    updateAllEmployeeData(context, payload) {
      context.commit('updateAllEmployeeData', payload)
    },
  },
  modules: {
  }
})
