export const skillsData = [
  { skill: "Java", rating: 0, isChecked: false },
  { skill: "C", rating: 0, isChecked: false },
  { skill: "C++", rating: 0, isChecked: false },
  { skill: "C#", rating: 0, isChecked: false },
  { skill: "HTML", rating: 0, isChecked: false },
  { skill: "ReactJS", rating: 0, isChecked: false },
  { skill: "CSS", rating: 0, isChecked: false },
  { skill: "JavaScript", rating: 0, isChecked: false },
  { skill: "Bootstrap", rating: 0, isChecked: false },
  { skill: "VueJS", rating: 0, isChecked: false },
  { skill: "AngularJS", rating: 0, isChecked: false },
  { skill: "Spring", rating: 0, isChecked: false },
  { skill: "Spring Boot", rating: 0, isChecked: false },
  { skill: "Python", rating: 0, isChecked: false },
  { skill: "JDBC", rating: 0, isChecked: false },
  { skill: "SQL", rating: 0, isChecked: false },
  { skill: "MongoDB", rating: 0, isChecked: false },
  { skill: "NodeJS", rating: 0, isChecked: false },
  { skill: "ExpressJS", rating: 0, isChecked: false },
  { skill: "J2EE", rating: 0, isChecked: false },
];

export const languages = [
  { skill: "Java", rating: 0, isChecked: false },
  { skill: "C", rating: 0, isChecked: false },
  { skill: "C++", rating: 0, isChecked: false },
  { skill: "C#", rating: 0, isChecked: false },
  { skill: "HTML", rating: 0, isChecked: false },
  { skill: "ReactJS", rating: 0, isChecked: false },
  { skill: "CSS", rating: 0, isChecked: false },
  { skill: "JavaScript", rating: 0, isChecked: false },
  { skill: "Bootstrap", rating: 0, isChecked: false },
  { skill: "VueJS", rating: 0, isChecked: false },
  { skill: "AngularJS", rating: 0, isChecked: false },
  { skill: "Spring", rating: 0, isChecked: false },
  { skill: "Spring Boot", rating: 0, isChecked: false },
  { skill: "Python", rating: 0, isChecked: false },
  { skill: "JDBC", rating: 0, isChecked: false },
  { skill: "SQL", rating: 0, isChecked: false },
  { skill: "MongoDB", rating: 0, isChecked: false },
  { skill: "NodeJS", rating: 0, isChecked: false },
  { skill: "ExpressJS", rating: 0, isChecked: false },
  { skill: "J2EE", rating: 0, isChecked: false },
]
export const technology = [
  { skill: "Servlet", rating: 0, isChecked: false },
  { skill: "JSP", rating: 0, isChecked: false },
  { skill: "JDBC", rating: 0, isChecked: false },
]

export const frontend = [
  { skill: "AngularJS", rating: 0, isChecked: false },
  { skill: "ReactJS", rating: 0, isChecked: false },
  { skill: "VueJS", rating: 0, isChecked: false },
  { skill: "CSS", rating: 0, isChecked: false },
  { skill: "Bootstrap", rating: 0, isChecked: false },
]

export const backend = [
  { skill: "Spring", rating: 0, isChecked: false },
  { skill: "Spring Boot", rating: 0, isChecked: false },
  { skill: "Python", rating: 0, isChecked: false },
  { skill: "JDBC", rating: 0, isChecked: false },
  { skill: "SQL", rating: 0, isChecked: false },
  { skill: "MongoDB", rating: 0, isChecked: false },
  { skill: "NodeJS", rating: 0, isChecked: false },
  { skill: "ExpressJS", rating: 0, isChecked: false },
  { skill: "J2EE", rating: 0, isChecked: false },
]

export const designpatterns = [
  { skill: "Singleton", rating: 0, isChecked: false },
  { skill: "Model View Controller (MVC)", rating: 0, isChecked: false },
  { skill: "Data Transfer Object (DTO)", rating: 0, isChecked: false },
  { skill: "Data Access Object (DAO) and Factory Pattern", rating: 0, isChecked: false },
]

export const rdbsapp = [
  { skill: "Oracle", rating: 0, isChecked: false },
  { skill: "MySQL", rating: 0, isChecked: false },
]

export const webservers = [
  { skill: "Apache Tomcat 8.0", rating: 0, isChecked: false },
]

export const aws = [
  { skill: "EC2", rating: 0, isChecked: false },
  { skill: "EBS", rating: 0, isChecked: false },
  { skill: "RDS", rating: 0, isChecked: false },
]

export const cdtools = [
  { skill: "SonarLint", rating: 0, isChecked: false },
  { skill: "Sonar Scanner", rating: 0, isChecked: false },
  { skill: "SonarQube & Crucible", rating: 0, isChecked: false },
]

export const vcsystem = [
  { skill: "Github", rating: 0, isChecked: false },
  { skill: "GitLab", rating: 0, isChecked: false },
  { skill: "BitBucket", rating: 0, isChecked: false },
]

export const batool = [
  { skill: "Maven", rating: 0, isChecked: false },
]

export const cicdtool = [
  { skill: "Jenkins", rating: 0, isChecked: false },
]

export const othertools = [
  { skill: "Tortise Git", rating: 0, isChecked: false },
  { skill: "Eclipse", rating: 0, isChecked: false },
  { skill: "STS", rating: 0, isChecked: false },
  { skill: "Fiddler", rating: 0, isChecked: false },
  { skill: "SQL Plus", rating: 0, isChecked: false },
  { skill: "MySQL Workbench", rating: 0, isChecked: false },
  { skill: "MS Office (Word, Excel & Power Point)", rating: 0, isChecked: false },
]

export const sdlc = [
  { skill: "Waterfall Model and Agile Git", rating: 0, isChecked: false },
]

export const skillArray = [
  {
    heading: 'Languages',
    data: languages
  }, {
    heading: 'J2EE Technologies',
    data: technology
  },
  {
    heading: 'Frameworks - Front End',
    data: frontend,
  },
  {
    heading: 'Frameworks- Back End',
    data: backend
  },
  {
    heading: 'Design Patterns',
    data: designpatterns,
  },
  {
    heading: 'RDBS Applications',
    data: rdbsapp,
  },
  {
    heading: 'Web Servers',
    data: webservers,
  },
  {
    heading: 'AWS',
    data: aws,
  },
 {
    heading: 'Code Quality Tools',
    data: cdtools,
  },
 {
    heading: 'Version Control System',
    data: vcsystem,
  },
  {
    heading: 'Build Automation Tool',
    data: batool,
  },
  {
    heading: 'CI/CD Tool',
    data: cicdtool,
  },
  {
    heading: 'Other Tools',
    data: othertools,
  },
  {
    heading: 'Software Development Processes (SDLC)',
    data: sdlc,
  },
];
export const projectTech = [
  {
    type: "Front End Technologies",
    data: frontend,
  },
  {
    type: 'Back End Technologies',
    data: backend
  },
  {
    type: 'RDBMS Application',
    data: rdbsapp,
  },
  {
    type: 'Tools Used',
    data: othertools,
  },
  {
    type: 'Others',
    data: designpatterns,
  }
];

export const techErrObj =[
  {
    heading: "Languages",
    errMsg: "",
  },
  {
    heading: "J2EE Technologies",
    errMsg: "",
  },
  {
    heading: "Frameworks - Front End",
    errMsg: "",
  },
  {
    heading: "Frameworks- Back End",
    errMsg: "",
  },
  {
    heading: "Design Patterns",
    errMsg: "",
  },
  {
    heading: "RDBS Applications",
    errMsg: "",
  },
  {
    heading: "Web Servers",
    errMsg: "",
  },
  {
    heading: "AWS",
    errMsg: "",
  },
  {
    heading: "Code Quality Tools",
    errMsg: "",
  },
  {
    heading: "Version Control System",
    errMsg: "",
  },
  {
    heading: "Build Automation Tool",
    errMsg: "",
  },
  {
    heading: "CI/CD Tool",
    errMsg: "",
  },
  {
    heading: "Other Tools",
    errMsg: "",
  },
  {
    heading: "Software Development Processes (SDLC)",
    errMsg: "",
  },
];


