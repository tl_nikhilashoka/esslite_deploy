export const courses = [
    {
        name: 'B.E',
        value: 'B.E'
    },
    {
        name: 'B.C.A',
        value: 'B.C.A'
    },
    {
        name: 'M.C.A',
        value: 'M.C.A'
    },
    {
        name: 'M.Tech',
        value: 'M.Tech'
    }
]