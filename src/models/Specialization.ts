export const specialization = [
    {
        name: 'ISE',
        value: 'ISE'
    },
    {
        name: 'CSE',
        value: 'CSE'
    },
    {
        name: 'EC',
        value: 'EC'
    },
    {
        name: 'ME',
        value: 'ME'
    }
]