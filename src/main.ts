import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faEdit, faPlusCircle, faTimesCircle, faSave, faTrashAlt, faUserGraduate, faCamera, faPlus, faAngleUp, faAngleDown, faTimes, faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
library.add(faEdit)
library.add(faTimesCircle)
library.add(faTrashAlt)
library.add(faPlusCircle)
library.add(faSave)
library.add(faUserGraduate)
library.add(faCamera)
library.add(faPlus)
library.add(faAngleUp)
library.add(faAngleDown)
library.add(faTimes)
library.add(faChevronLeft)
library.add(faChevronRight)

const app = createApp(App, { myProp: "value" })
app.component('font-awesome-icon', FontAwesomeIcon)
app.use(store).use(router).mount('#app')
